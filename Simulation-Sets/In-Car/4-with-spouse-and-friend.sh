#!/bin/bash
#    Simulation Set: 4 - With Spouse and Friend
#    ------------------------------------------------------------------------
#    Author:      David J. Sanders
#    Student No:  H00035340
#    Date:        12 Apr 2016
#    ------------------------------------------------------------------------
#    Overivew:   
#                Bob's phone is phone-43132
#                Sue's phone is phone-43133
#                Andrew's phone is phone-43134
#
#
#    Revision History
#    --------------------------------------------------------------------------
#    Date         | By             | Reason
#    --------------------------------------------------------------------------
#    12 Apr 2016  | D Sanders      | Revised structure for simulations.
#
#
#================= Scenario Init - Validate $simpath exists ====================
#
# Simulation Setup
#
if [ "X"$simpath == "X" ]; then
    echo "ERROR: simpath is not defined!"
    echo ""
    echo "Before running this script, ensure that simpath is defined:"
    echo ""
    echo "  export simpath=/path/to/studySim"
    echo
    exit 1
fi

simulation="4"
simulation_includes=$simpath/Simulation-Sets/Notifications/includes
scenario_includes=$simpath/Scenario-Setup/In-Car-Notifications/includes

source $simpath/includes/check_params.sh
source $simpath/includes/setup.sh
source $simpath/includes/set_version.sh
source $simpath/includes/set_outputs.sh
source $scenario_includes/additional_ports.sh
#
# Simulation 2 Configuration
#
sim_heading="Simulation 1: Sensitive notifications with different people present."
over_view="Bob is in the car with his wife Sue. They are travelling to work. "
over_view=$over_view"Bob has a coronary condition and uses a smartphone app to "
over_view=$over_view"remind him to take medication, relax, to suggest exercises, "
over_view=$over_view"and to monitor his vital statistics. "
over_view=$over_view"\n\n"
over_view=$over_view"During the journey Bob receives a number of notifications "
over_view=$over_view"from his medical app. When he is with Sue, he is happy "
over_view=$over_view"for her to hear the notifications too; when he is with "
over_view=$over_view"others though, he does not want his condition to be "
over_view=$over_view"disclosed.\n\n"
over_view=$over_view"Bob and Sue are joined by Andrew during the journey.\n"
over_view=$over_view"Andrew is defined as a friend of Bob.\n"

echo
echo "Simulation Overview"
echo "==================="
printf "$over_view"
echo

pause "Please ensure setup has been run before the simulation "

set +e
start_message "${sim_heading}"

# Bob sits with Jing and can see the phone screen
let test_id=test_id+1
pre_test $test_id "Bob turns on his phone."
# Phone Screen must be version 3
start_phone Bob "v3_00" $phoneRedisPort

let test_id=test_id+1
do_log "Log Bob phone screen started." $test_id

# If Presence engine is available, set it up.
container_name=""$presentAs"_presence_"$presencePort
check_docker "${container_name}"   # sets $DOCKER_CHECK
if ! [ "X" == "${DOCKER_CHECK}" ]; then
    echo "*** PRESENCE ENGINE EXISTS ***"
    # Connect to the presence engine
    let test_id=test_id+1
    presence_engine='"presence-engine":"'$serverIPName':'$presencePort'/'$presentAs'"'
    user_id='"user-id":"Bob"'
    data='{'$genKey', '$presence_engine', '$user_id'}'
    do_post "${data}" \
            $phonePort \
            "/"$presentAs"/config/presence" \
            "Bob's phone is connected to the presence system." \
            $test_id

    let test_id=test_id+1
    do_log "SIMENGINE Setting Sue as Bob's spouse." $test_id

    # Set Sue up as Bob's spouse
    let test_id=test_id+1
    relationship='"relationship":"spouse"'
    data='{'$genKey', '$relationship'}'
    do_post "${data}" \
            $presencePort \
            "/"$presentAs"/people/Bob/Sue" \
            "Sue is defined in presence as spouse of Bob" \
            $test_id

    let test_id=test_id+1
    do_log "SIMENGINE Setting Andrew as a friend of Bob." $test_id

    # Set Andrew up as Bob's colleague
    let test_id=test_id+1
    relationship='"relationship":"friend"'
    data='{'$genKey', '$relationship'}'
    do_post "${data}" \
            $presencePort \
            "/"$presentAs"/people/Bob/Andrew" \
            "Andrew is defined in presence as friend of Bob" \
            $test_id
fi

# If context is available, configure it.
container_name=""$presentAs"_context_"$contextPort
check_docker "${container_name}"   # sets $DOCKER_CHECK
if ! [ "X" == "${DOCKER_CHECK}" ]; then
    echo "*** CONTEXT ENGINE EXISTS ***"
    # Connect Bob's phone to the context engine
    let test_id=test_id+1
    context='"context-engine":"'$serverIPName':'$contextPort'/'$presentAs'/subscribe"'
    data='{'$genKey', '$context'}'
    do_put "${data}" \
           $phonePort \
           "/"$presentAs"/config/context" \
           "Connect Bob's phone to the context engine at "$serverIPName":"$contextPort \
           $test_id
fi

source $simpath/Simulation-Sets/In-Car/with-someone-steps.sh
