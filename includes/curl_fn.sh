function do_log {
    # $1 - message
    # $2 - Test Number

    tmp_file=`date +%d%m%Y%H%M%S%N`
    tmp_dir='/tmp/'
    output_file=$tmp_dir"curl-output-"${tmp_file}".txt"
    header_file=$tmp_dir"curl-header-"${tmp_file}".txt"
    sender='"sender":"SIM-ENGINE"'
    logtype='"log-type":"normal"'
    message='"message":"*** SIMENGINE '$1'"'
    data='{'$genKey', '$logtype', '$sender', '$message'}'

    pre_test $2 "${1}"

    let limit=5
    timeout="YES"

    for counter in `seq 1 1 ${limit}`;
    do
        curl -s -o ${output_file} \
             -D ${header_file} \
             -X POST -d "${data}" \
             --connect-timeout 20 \
             $serverName:$loggerPort/$presentAs/log

        if grep -i "Gateway" ${header_file} ; then
            echo "Gateway timeout. Retrying."
            sleep 5
        else
            timeout="NO"
            cat ${output_file}
            echo
            echo
            echo "done."
            break
        fi
    done
    if [ "${timeout}" == "YES" ]; then
        echo "Unable to complete request. Timeout after ${counter} attempts"
    fi

    rm ${output_file}
    rm ${header_file}

    post_test
}

function do_post {
    # $1 - data string
    # $2 - portNumber
    # $3 - URL
    # $4 - Heading
    # $5 - Test Number
    tmp_file=`date +%d%m%Y%H%M%S%N`
    tmp_dir='/tmp/'
    output_file=$tmp_dir"curl-output-"${tmp_file}".txt"
    header_file=$tmp_dir"curl-header-"${tmp_file}".txt"

    pre_test $5 "${4}"
    let limit=5
    timeout="YES"

    for counter in `seq 1 1 ${limit}`;
    do
        curl -s -o ${output_file} \
             -D ${header_file} \
             -X POST -d "${1}" \
             --connect-timeout 20 \
             $serverName:$2$3

        if grep -i "Gateway" ${header_file} ; then
            echo "Gateway timeout. Retrying."
            sleep 5
        else
            timeout="NO"
            cat ${output_file}
            echo
            echo
            echo "done."
            break
        fi
    done
    if [ "${timeout}" == "YES" ]; then
        echo "Unable to complete request. Timeout after ${counter} attempts"
    fi

    rm ${output_file}
    rm ${header_file}

    post_test
}

function do_put {
    # $1 - data string
    # $2 - portNumber
    # $3 - URL
    # $4 - Heading
    # $5 - Test Number
    tmp_file=`date +%d%m%Y%H%M%S%N`
    tmp_dir='/tmp/'
    output_file=$tmp_dir"curl-output-"${tmp_file}".txt"
    header_file=$tmp_dir"curl-header-"${tmp_file}".txt"

    pre_test $5 "${4}"
    let limit=5
    timeout="YES"

    for counter in `seq 1 1 ${limit}`;
    do
        curl -s -o ${output_file} \
             -D ${header_file} \
             -X PUT -d "${1}" \
             --connect-timeout 20 \
             $serverName:$2$3

        if grep -i "Gateway" ${header_file} ; then
            echo "Gateway timeout. Retrying."
            sleep 5
        else
            timeout="NO"
            cat ${output_file}
            echo
            echo
            echo "done."
            break
        fi
    done
    if [ "${timeout}" == "YES" ]; then
        echo "Unable to complete request. Timeout after ${counter} attempts"
    fi

    rm ${output_file}
    rm ${header_file}

    post_test
}

function do_delete {
    # $1 - data string
    # $2 - portNumber
    # $3 - URL
    # $4 - Heading
    # $5 - Test Number
    tmp_file=`date +%d%m%Y%H%M%S%N`
    tmp_dir='/tmp/'
    output_file=$tmp_dir"curl-output-"${tmp_file}".txt"
    header_file=$tmp_dir"curl-header-"${tmp_file}".txt"

    pre_test $5 "${4}"
    let limit=5
    timeout="YES"

    for counter in `seq 1 1 ${limit}`;
    do
        curl -s -o ${output_file} \
             -D ${header_file} \
             -X DELETE -d "${1}" \
             --connect-timeout 20 \
             $serverName:$2$3

        if grep -i "Gateway" ${header_file} ; then
            echo "Gateway timeout. Retrying."
            sleep 5
        else
            timeout="NO"
            cat ${output_file}
            echo
            echo
            echo "done."
            break
        fi
    done
    if [ "${timeout}" == "YES" ]; then
        echo "Unable to complete request. Timeout after ${counter} attempts"
    fi

    rm ${output_file}
    rm ${header_file}

    post_test
}

function do_get {
    # $1 - data string
    # $2 - portNumber
    # $3 - URL
    # $4 - Heading
    # $5 - Test Number
    tmp_file=`date +%d%m%Y%H%M%S%N`
    tmp_dir='/tmp/'
    output_file=$tmp_dir"curl-output-"${tmp_file}".txt"
    header_file=$tmp_dir"curl-header-"${tmp_file}".txt"

    pre_test $5 "${4}"
    let limit=5
    timeout="YES"

    for counter in `seq 1 1 ${limit}`;
    do
        curl -s -o ${output_file} \
             -D ${header_file} \
             -X GET -d "${1}" \
             --connect-timeout 20 \
             $serverName:$2$3

        if grep -i "Gateway" ${header_file} ; then
            echo "Gateway timeout. Retrying."
            sleep 5
        else
            timeout="NO"
            cat ${output_file}
            echo
            echo
            echo "done."
            break
        fi
    done
    if [ "${timeout}" == "YES" ]; then
        echo "Unable to complete request. Timeout after ${counter} attempts"
    fi

    rm ${output_file}
    rm ${header_file}

    post_test
}
