#
# References:
# 1. http://stackoverflow.com/questions/16483119/example-of-how-to-use-getopts-in-bash
# 2. http://stackoverflow.com/questions/806906/how-do-i-test-if-a-variable-is-a-number-in-bash
#
not_allowed=''
number_expression='^[0-9]+$'
save_param=''
docset=${simpath}'/doc/docsets/all-docs.inc'
let error_occurred=0

usage() { echo "Usage: $0 [-d /path/to/docset] [-h]" 1>&2; exit 1; }

while getopts "hd:" param; do
    case "$param" in
        d) if ! [ -e $OPTARG ]; then
               not_allowed="Invalid document set: ${OPTARG} does not exist"
               let error_occurred=1
           else
               docset=$OPTARG
           fi
           ;;
        h) usage
           ;;
        *) not_allowed="invalid option: -"$OPTARG
           let error_occurred=1
           break
           ;;
    esac
done
if [ "$error_occurred" -eq "1" ]; then
    echo "${0}: ${not_allowed}"
    echo
    usage
fi
tVersion=$version

