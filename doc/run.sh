#!/bin/bash
#    Script: run.sh
#    ------------------------------------------------------------------------
#    Author:      David J. Sanders
#    Student No:  H00035340
#    Date:        12 Apr 2016
#    ------------------------------------------------------------------------
#    Overivew:    
#
#    Revision History
#    --------------------------------------------------------------------------
#    Date         | By             | Reason
#    --------------------------------------------------------------------------
#    12 Apr 2016  | D Sanders      | Revised structure for simulations.
#
#
#================= Scenario Init - Validate $simpath exists ====================
#
# Simulation Setup
#
if [ "X"$simpath == "X" ]; then
    echo "ERROR: simpath is not defined!"
    echo ""
    echo "Before running this script, ensure that simpath is defined:"
    echo ""
    echo "  export simpath=/path/to/studySim"
    echo
    exit 1
fi

source $simpath/includes/check_doc_params.sh
source $simpath/includes/setup.sh

sim_heading="Setting up and starting documentation servers and test servers."
start_message "${sim_heading}"

source $docset

let loop_counter=1
for name in ${name_array[@]}
do
    current_ver="${ver_array[loop_counter-1]}"
    current_port="${port_array[loop_counter-1]}"
    current_doc_port="${doc_port_array[loop_counter-1]}"
    current_image="${image_array[loop_counter-1]}"

    let test_id=test_id+1
    pre_test $test_id "${name} on port ${current_port} with image mscit_${current_image}"
    echo -n "Starting ${name}: "
    $simpath/run-docker.sh \
        -c dsanderscan/mscit_${current_image} \
        -n ${name}"_"${current_ver} \
        -p ${current_port} \
        -v ${current_ver} \
        -d
    echo

    let test_id=test_id+1
    pre_test $test_id "Doc_${name} on port ${current_doc_port} with image mscitdoc_${current_image}"
    echo -n "Starting doc server for ${name}: "
    $simpath/run-docker.sh \
        -c dsanderscan/mscitdoc_${current_image} \
        -n "Doc_"${name}"_"${current_ver} \
        -p ${current_doc_port} \
        -v ${current_ver} \
        -d
    echo
    let loop_counter+=1
done

#
# Originally intended to warm-up servers. Didn't have intended impact.
# to be revisited in future.
#
#let test_id=test_id+1
#pre_test $test_id "Pausing to let services start"
#let loop_counter=1
#for name in ${name_array[@]}
#do
#    sleep 1
#done
#
#let test_id=test_id+1
#pre_test $test_id "Documentation servers running. Warming up..."
#let loop_counter=1
#for name in ${name_array[@]}
#do
#    current_ver="${ver_array[loop_counter-1]}"
#    current_port="${port_array[loop_counter-1]}"
#    current_doc_port="${doc_port_array[loop_counter-1]}"
#    current_image="${image_array[loop_counter-1]}"
#
#    echo "Warming up http://localhost:${current_doc_port}/${current_ver}/ui"
#    curl -s -o /tmp/docsrv-warmup.tmp "http://localhost:${current_doc_port}/${current_ver}/ui/#" ; echo
#    let loop_counter+=1
#done

let test_id=test_id+1
pre_test $test_id "Documentation servers running. Stop command: \$simpath/doc/stop.sh"
echo
echo
echo "Summary of services"
echo "==================="
let loop_counter=1
for name in ${name_array[@]}
do
    current_ver="${ver_array[loop_counter-1]}"
    current_port="${port_array[loop_counter-1]}"
    current_doc_port="${doc_port_array[loop_counter-1]}"
    current_image="${image_array[loop_counter-1]}"

    echo "${name} : http://localhost:${current_port}/${current_ver} (Doc server: http://localhost:${current_doc_port}/${current_ver}/ui)"
    let loop_counter+=1
done
echo
stop_message "${sim_heading}"

