#!/bin/bash
echo "Pulling dsanderscan/mscitdoc_v3_00_bluetooth"
docker pull dsanderscan/mscitdoc_v3_00_bluetooth
echo "Pulling dsanderscan/mscitdoc_v3_01_bluetooth"
docker pull dsanderscan/mscitdoc_v3_01_bluetooth
echo "Pulling dsanderscan/mscitdoc_v3_00_location_service"
docker pull dsanderscan/mscitdoc_v3_00_location_service
echo "Pulling dsanderscan/mscitdoc_v3_00_logger"
docker pull dsanderscan/mscitdoc_v3_00_logger
echo "Pulling dsanderscan/mscitdoc_v3_00_monitor_app"
docker pull dsanderscan/mscitdoc_v3_00_monitor_app
echo "Pulling dsanderscan/mscitdoc_v3_01_monitor_app"
docker pull dsanderscan/mscitdoc_v3_01_monitor_app
echo "Pulling dsanderscan/mscitdoc_v4_00_monitor_app"
docker pull dsanderscan/mscitdoc_v4_00_monitor_app
echo "Pulling dsanderscan/mscitdoc_v3_00_notification"
docker pull dsanderscan/mscitdoc_v3_00_notification
echo "Pulling dsanderscan/mscitdoc_v4_00_notification"
docker pull dsanderscan/mscitdoc_v4_00_notification
echo "Pulling dsanderscan/mscitdoc_v3_00_phone"
docker pull dsanderscan/mscitdoc_v3_00_phone
echo "Pulling dsanderscan/mscitdoc_v3_01_phone"
docker pull dsanderscan/mscitdoc_v3_01_phone
echo "Pulling dsanderscan/mscitdoc_v4_00_phone"
docker pull dsanderscan/mscitdoc_v4_00_phone
echo "Pulling dsanderscan/mscitdoc_v4_00_context"
docker pull dsanderscan/mscitdoc_v4_00_context

