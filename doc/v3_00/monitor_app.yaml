# this is an example of the Uber API
# as a demonstration of an API spec in YAML
swagger: '2.0'
info:
  title: Monitor App (v3_00)
  version: "3.00"
  contact:
    name: David J. Sanders, University of Liverpool
    email: dsanderscanada@gmail.com
  description: |
               The Monitor App models an application designed by Besoain, et. al, (2015) which promotes positive behaviour reinforcement for men seeking men, to reduce the spread of sexually transmitted infections (STIs).
               
               The service has two modes of operation. First, it is informed that an application has launched and the application is checked against a 'watch' list. Second, the app detects the user entering into a hot spot area and issues notifications.
               
               In v3.00 the notification is taken from Besoain, et al. (2015), and states for app lauches * "The applciation {X} has been launched. Remember Safe sex is good sex!" * and for hot spots * "You have entered an area known for high rates of STI infections and drug use. Remember Safe sex is good sex!" * The service models the app and the notifications are issued without regard to context or obfuscation.
               
               The service is normally run as part of a simulation, but can be run multiple times independently too. When running multiple instances, remember that port numbers need to be unique. Also, if data needs persisting, the -s (save) option should be used (see Reference 1 for more information).
               
               To run the service and use the <em>Try it out!</em> buttons on this page, use the run-docker.sh script in the studySim repro. Make sure the environment variable simpath is set (export simpath=/path/to/studySim) and, for best effect, run its own window (if using a GUI).
               
               Execute the command below to run the container
               
               <ul>
                 <li><em>
                 $simpath/run-docker.sh -c dsanderscan/mscit_v3_00_monitor_app -n Monitor_App -p 5003 -v v3_00
                 </em></li>
               </ul>
               
               Optional Parameters
               <ul>
                 <li>-d : run as a daemon</li>
                 <li>-p : set the port number to use</li>
                 <li>-s : set the path to save (persist) data</li>
                 <li>-z : specify the name the server should present itself as</li>
                 <li>-v : set the version to present as</li>
               </ul>

               If run as above, the service will stop and remove its containers when you ^c (control-c) the app. If the container is run as a daemon, the run-docker.sh script will provide commands to stop and remove the daemon.
               
               **References**
               <ol>
                 <li>Besoain, F. et al., (2015). Prevention of sexually transmitted infections using mobile devices and ubiquitous computing. <em>International Journal of Health Geographics</em>, 14(1), pp. 1-12.</li>
                 <li>Docker (n.d.) 'Manage data in containers' [ONLINE]. Available at: https://docs.docker.com/engine/userguide/containers/dockervolumes/ (Accessed: 09 March 2016)</li>
               </ol>
               
               **Notes**
               <ul>
                 <li>Central logging operations (/config/logger) are supported in this service. The routes are **not** documented here; see the config logger service information for more details.</li>
               </ul>
host: localhost:5003
# array of all schemes that your API supports
schemes:
  - http
# will be prefixed to all paths
basePath: /v3_00
produces:
  - application/json
consumes: 
  - application/json
paths:
#
# /state
#
  /state:
    get:
      summary: Check monitoring app state (on or off).
      tags: 
        - State
      responses:
        200:
          description: Success.
          schema:
            $ref: '#/definitions/State_Output'
        default:
          description: Another error occured
          schema:
            $ref: '#/definitions/ErrorObject'
#
# /state{state}
#
  /state/{state}:
    parameters:
      - name: state
        in: path
        description: The state (on or off) of the monitor app. Any value other than on or off is rejected at the boundary level.
        required: true
        type: string
    post:
      summary: Set Monitor App state (on or off).
      description: Set the Monitor App service state to on or off. When setting to on, requires a specific schema of JSON data to be provided.
      parameters:
        - name: body
          in: body
          description: The data required to modify the state of the Monitor App service.
          schema:
            $ref: '#/definitions/State_Post_Params'
          required: true
      tags: 
        - State
      responses:
        200:
          description: Success.
          schema:
            $ref: '#/definitions/State_Output'
        default:
          description: Another error occured
          schema:
            $ref: '#/definitions/ErrorObject'
#
# /apps
#
  /apps:
    get:
      summary: Get the list of applications (or apps) being monitored by this Monitor App service.
      tags: 
        - Apps
      responses:
        200:
          description: Success.
          schema:
            $ref: '#/definitions/Apps_Output'
        404:
          description: No applications are being monitored.
          schema:
            $ref: '#/definitions/ErrorObject'
        default:
          description: Another error occured
          schema:
            $ref: '#/definitions/ErrorObject'

  #
  # /app/{app}
  #
  /app/{app}:
    parameters:
      - name: app
        in: path
        description: The name of an application (or app) whose monitoring state is to be modified.
        required: true
        type: string
    get:
      summary: Validate if the application (or app) is being monitored by this Monitor App service.
      tags: 
        - Apps
      responses:
        200:
          description: Success.
          schema:
            $ref: '#/definitions/App_Output'
        404:
          description: The application is not being monitored.
          schema:
            $ref: '#/definitions/ErrorObject'
        default:
          description: Another error occured
          schema:
            $ref: '#/definitions/ErrorObject'
    post:
      summary: Set the Monitor App service state to on or off.
      description: Set the Monitor App service state to on or off. When setting to on, requires a specific schema of JSON data to be provided.
      parameters:
        - name: body
          in: body
          description: The data reqiured to modify the state of the Monitor App service.
          schema:
            $ref: '#/definitions/App_Post_Params'
          required: true
      tags: 
        - Apps
      responses:
        200:
          description: Success.
          schema:
            $ref: '#/definitions/State_Output'
        default:
          description: Another error occured
          schema:
            $ref: '#/definitions/ErrorObject'
    delete:
      summary: Stop the app from being monitored.
      description: Set the Monitor App service to stop monitoring the app - any launches of the app after this point will be ignored.
      parameters:
        - name: body
          in: body
          description: The data reqiured to modify the state of the app being monitored by the Monitor App service.
          schema:
            $ref: '#/definitions/App_Delete_Params'
          required: true
      tags: 
        - Apps
      responses:
        200:
          description: Success.
          schema:
            $ref: '#/definitions/State_Output'
        404:
          description: The application is not being monitored.
          schema:
            $ref: '#/definitions/ErrorObject'
        default:
          description: Another error occured
          schema:
            $ref: '#/definitions/ErrorObject'
#
# /launched
#
  /launched/{app}:
    parameters:
      - name: app
        in: path
        description: The name of an application (or app) that has been launched.
        required: true
        type: string
    post:
      summary: Notify the Monitor App service that an application has been launched.
      description: Notify the Monitor App service that an application has been launched, typically sent by the recipient (i.e., the phone) to the Monitor App service. If the app is not on the monitored list (i.e., watched), it will be ignored. The service MUST be on and configured being called, otherwise the launch notification will return a 400 error.
      tags: 
        - Launched
      responses:
        200:
          description: Success.
          schema:
            $ref: '#/definitions/Launched_Output'
        400:
          description: The application launch was received but nothing could be done about it. So a 400 is returned which can be ignored by the calling service.
          schema:
            $ref: '#/definitions/Launched_Error'
        404:
          description: The application launch was received but the app is not being monitored (or watched). So a 404 is returned which can be ignored by the calling service.
          schema:
            $ref: '#/definitions/Launched_Error'
        default:
          description: Another error occured
          schema:
            $ref: '#/definitions/Launched_Error'

#
# CONFIG
#
definitions:
#
# Common
#
  message:
    type: string
    description: An expanded message from the service
  status:
    type: string
    description: HTTP Response Code
  response:
    type: string
    description: The message (or response) returned from the service

  ErrorObject:
    type: object
    properties:
      message:
        $ref: '#/definitions/message'
      status:
        $ref: '#/definitions/status'
      response:
        $ref: '#/definitions/response'
      data:
        type: string
        description: The data returned from the service.

  key:
    type: string
    description: The control key to validate permission to modify or perform an operation.

#
# /state AND /state/{state}
#
  recipient:
    type: string
    description: The recipient the service will notify (i.e., the phone). NOTE. this is the full URL to the notification receiver of the phone, e.g. http://127.0.0.1:43129/v1_00/notification. At the current release IT MUST be an ip address as the Docker container needs to be able to address it and there is no DNS/hosts entry.

  notification-service:
    type: string
    description: The notification service this service will use to notify the recipient (i.e., the phone). NOTE. this is the full URL to the notification service, e.g. http://127.0.0.1:43125/v1_00/notification. At the current release IT MUST be an ip address as the Docker container needs to be able to address it and there is no DNS/hosts entry. NOTE> This is not the same as the recipient. This is the service that will issue the notification to the recipient on behalf of this service.

  location-service:
    type: string
    description: The location checking service this service will use to validate if a location is within a hotspot or not. NOTE. this is the full URL to the notification service, e.g. http://127.0.0.1:43127/v1_00/check. At the current release IT MUST be an ip address as the Docker container needs to be able to address it and there is no DNS/hosts entry. NOTE> This is the service that will check the coordinates against known hot spots on behalf of this service.

  location-provider:
    type: string
    description: The service gets the current location from a location provider, typically the emulated GPS within a phone and stores this as a set of X,Y coordinates. NOTE. this is the full URL to the location provider service and IS the SAME AS the recipient EXCEPT the path changes in the URL, e.g. http://127.0.0.1:43129/v1_00/location. At the current release IT MUST be an ip address as the Docker container needs to be able to address it and there is no DNS/hosts entry. NOTE> This IS the SAME AS the recipient EXCEPT for the path.

  state:
    type: string
    description: The state of the monitor app which is either on or off.
    
  State_Data:
    type: object
    description: The data returned from the service.
    properties:
      recipient:
        $ref: '#/definitions/recipient'
      notification-service:
        $ref: '#/definitions/notification-service'
      state:
        $ref: '#/definitions/state'
      location-service:
        $ref: '#/definitions/location-service'
      location-provider:
        $ref: '#/definitions/location-provider'

  State_Output:
    type: object
    properties:
      message:
        $ref: '#/definitions/message'
      status:
        $ref: '#/definitions/status'
      response:
        $ref: '#/definitions/response'
      data:
          $ref: '#/definitions/State_Data'

  State_Post_Params:
    type: object
    description: The data required to modify the state of the Monitor App service.
    properties:
      key:
        $ref: '#/definitions/key'
      recipient:
        $ref: '#/definitions/recipient'
      notification-service:
        $ref: '#/definitions/notification-service'
      location-service:
        $ref: '#/definitions/location-service'
      location-provider:
        $ref: '#/definitions/location-provider'

#
# /apps AND /app/{app}
#
  app:
    type: string
    description: The name of the app - NB All app names are converted to UPPERCASE and are *NOT* case sensitive.
    
  app-description:
    type: string
    description: The textual description of the app.

  app-data:
    type: object
    description: The data returned from the service.
    properties:
      app:
        $ref: '#/definitions/app'
      description:
        $ref: '#/definitions/app-description'

  Apps_Output:
    type: object
    properties:
      message:
        $ref: '#/definitions/message'
      status:
        $ref: '#/definitions/status'
      response:
        $ref: '#/definitions/response'
      data:
        type: array
        description: The list of apps being monitored
        items:
          $ref: '#/definitions/app-data'

  App_Output:
    type: object
    properties:
      message:
        $ref: '#/definitions/message'
      status:
        $ref: '#/definitions/status'
      response:
        $ref: '#/definitions/response'
      data:
        $ref: '#/definitions/app-data'

  App_Post_Params:
    type: object
    properties:
      key:
        $ref: '#/definitions/key'
      description:
        $ref: '#/definitions/app-description'

  App_Delete_Params:
    type: object
    properties:
      key:
        $ref: '#/definitions/key'

#
# /launched/{app}
#
  Launched_Output:
    type: object
    properties:
      message:
        $ref: '#/definitions/message'
      status:
        $ref: '#/definitions/status'
      response:
        $ref: '#/definitions/response'
      data:
        type: string
        description: A null string indicates success

  Launched_Error:
    type: object
    properties:
      message:
        $ref: '#/definitions/message'
      status:
        $ref: '#/definitions/status'
      response:
        $ref: '#/definitions/response'
      data:
        type: object
        properties:
          recipient:
            $ref: '#/definitions/recipient'
          notification-service:
            $ref: '#/definitions/notification-service'

