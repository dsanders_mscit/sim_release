#!/bin/bash
#    Script: run.sh
#    ------------------------------------------------------------------------
#    Author:      David J. Sanders
#    Student No:  H00035340
#    Date:        12 Apr 2016
#    ------------------------------------------------------------------------
#    Overivew:    
#
#    Revision History
#    --------------------------------------------------------------------------
#    Date         | By             | Reason
#    --------------------------------------------------------------------------
#    12 Apr 2016  | D Sanders      | Revised structure for simulations.
#
#
#================= Scenario Init - Validate $simpath exists ====================
#
# Simulation Setup
#
if [ "X"$simpath == "X" ]; then
    echo "ERROR: simpath is not defined!"
    echo ""
    echo "Before running this script, ensure that simpath is defined:"
    echo ""
    echo "  export simpath=/path/to/studySim"
    echo
    exit 1
fi

source $simpath/includes/check_doc_params.sh
source $simpath/includes/setup.sh

pause "Documentations servers about to be stopped for ALL users"

sim_heading="Stopping documentation servers and test servers."
start_message "${sim_heading}"

source "$docset"

let loop_counter=1
for name in ${name_array[@]}
do
    current_ver="${ver_array[loop_counter-1]}"
    current_port="${port_array[loop_counter-1]}"
    current_doc_port="${doc_port_array[loop_counter-1]}"
    current_image="${image_array[loop_counter-1]}"

    let test_id=test_id+1
    pre_test $test_id "Stopping ${name} on port ${current_port} with image mscit_${current_image}"
    echo -n "Stopping ${name}: "
    docker kill ${name}"_"${current_ver}
    echo -n "Removing ${name}: "
    docker rm ${name}"_"${current_ver}

    echo
    let test_id=test_id+1
    pre_test $test_id "Stopping ${name} on port ${current_doc_port} with image mscitdoc_${current_image}"
    echo -n "Stopping doc server for ${name}: "
    docker kill "Doc_"${name}"_"${current_ver}
    echo -n "Removing doc server ${name}: "
    docker rm "Doc_"${name}"_"${current_ver}
    echo

    let loop_counter+=1
done

# End simulation
let test_id=test_id+1
pre_test $test_id "Documentation servers stopped for ALL users. Restart with: \$simpath/doc/run.sh"
echo
stop_message "${sim_heading}"


