#!/bin/bash
current=$(pwd)
cd $simpath/doc/v3_00/bluetooth
echo "Building dsanderscan/mscitdoc_v3_00_bluetooth"
docker build -t dsanderscan/mscitdoc_v3_00_bluetooth .
echo

cd $simpath/doc/v3_01/bluetooth
echo "Building dsanderscan/mscitdoc_v3_01_bluetooth"
docker build -t dsanderscan/mscitdoc_v3_01_bluetooth .
echo

cd $simpath/doc/v3_00/location_service
echo "Building dsanderscan/mscitdoc_v3_00_location_service"
docker build -t dsanderscan/mscitdoc_v3_00_location_service .
echo

cd $simpath/doc/v3_00/logger
echo "Building dsanderscan/mscitdoc_v3_00_logger"
docker build -t dsanderscan/mscitdoc_v3_00_logger .
echo

cd $simpath/doc/v3_00/monitor_app
echo "Building dsanderscan/mscitdoc_v3_00_monitor_app"
docker build -t dsanderscan/mscitdoc_v3_00_monitor_app .
echo

cd $simpath/doc/v3_01/monitor_app
echo "Building dsanderscan/mscitdoc_v3_01_monitor_app"
docker build -t dsanderscan/mscitdoc_v3_01_monitor_app .
echo

cd $simpath/doc/v4_00/monitor_app
echo "Building dsanderscan/mscitdoc_v4_00_monitor_app"
docker build -t dsanderscan/mscitdoc_v4_00_monitor_app .
echo

cd $simpath/doc/v3_00/notification_service
echo "Building dsanderscan/mscitdoc_v3_00_notification"
docker build -t dsanderscan/mscitdoc_v3_00_notification .
echo

cd $simpath/doc/v4_00/notification_service
echo "Building dsanderscan/mscitdoc_v4_00_notification"
docker build -t dsanderscan/mscitdoc_v4_00_notification .
echo

cd $simpath/doc/v3_00/phone
echo "Building dsanderscan/mscitdoc_v3_00_phone"
docker build -t dsanderscan/mscitdoc_v3_00_phone .
echo

cd $simpath/doc/v3_01/phone
echo "Building dsanderscan/mscitdoc_v3_01_phone"
docker build -t dsanderscan/mscitdoc_v3_01_phone .
echo

cd $simpath/doc/v4_00/phone
echo "Building dsanderscan/mscitdoc_v4_00_phone"
docker build -t dsanderscan/mscitdoc_v4_00_phone .
echo

cd $simpath/doc/v4_00/context
echo "Building dsanderscan/mscitdoc_v4_00_context"
docker build -t dsanderscan/mscitdoc_v4_00_context .
echo

cd ${current}

