#!/bin/bash
echo "Pushing dsanderscan/mscitdoc_v3_00_bluetooth"
docker push dsanderscan/mscitdoc_v3_00_bluetooth
echo "Pushing dsanderscan/mscitdoc_v3_01_bluetooth"
docker push dsanderscan/mscitdoc_v3_01_bluetooth
echo "Pushing dsanderscan/mscitdoc_v3_00_location_service"
docker push dsanderscan/mscitdoc_v3_00_location_service
echo "Pushing dsanderscan/mscitdoc_v3_00_logger"
docker push dsanderscan/mscitdoc_v3_00_logger
echo "Pushing dsanderscan/mscitdoc_v3_00_monitor_app"
docker push dsanderscan/mscitdoc_v3_00_monitor_app
echo "Pushing dsanderscan/mscitdoc_v3_01_monitor_app"
docker push dsanderscan/mscitdoc_v3_01_monitor_app
echo "Pushing dsanderscan/mscitdoc_v4_00_monitor_app"
docker push dsanderscan/mscitdoc_v4_00_monitor_app
echo "Pushing dsanderscan/mscitdoc_v3_00_notification"
docker push dsanderscan/mscitdoc_v3_00_notification
echo "Pushing dsanderscan/mscitdoc_v4_00_notification"
docker push dsanderscan/mscitdoc_v4_00_notification
echo "Pushing dsanderscan/mscitdoc_v3_00_phone"
docker push dsanderscan/mscitdoc_v3_00_phone
echo "Pushing dsanderscan/mscitdoc_v3_01_phone"
docker push dsanderscan/mscitdoc_v3_01_phone
echo "Pushing dsanderscan/mscitdoc_v4_00_phone"
docker push dsanderscan/mscitdoc_v4_00_phone
echo "Pushing dsanderscan/mscitdoc_v4_00_context"
docker push dsanderscan/mscitdoc_v4_00_context

