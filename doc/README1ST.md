#
# Doc Servers

The documentation servers provide Docker images of Swagger servers for each of the models delivered by the research project. There is one server for each version and the model itself is run to allow the API to be tried using Swagger.    

**Note 1**, if you are connecting using SSH, port forwarding will need to be set to allow the documentation to be accessed by a local browser; e.g. ssh -L 8081:127.0.0.1:8081 (for the documentation server) and -L 5001:127.0.0.1:5001 (for the model server). If you are connecting via a GUI, directly to the host, then Firefox/Chrome/etc. can be used locally.

**Note 2**, If running the documentation servers on AWS or Azure, a minimum of 2GB RAM is required. On AWS, a t2.micro runs the framework really well; however, because it only has 1GB RAM, it does *not* run the full doc servers. Use the -d flag with the scripts below to specify which doc server set to load at a specific time (see $simpath/doc/docsets/*.inc).

The following commands start, stop, and (re)build the documentation libraries:

Command                 | Description
------------------------|-------------------------------------------------------
$simpath/doc/build.sh   | Build all documentation libraries into Docker containers
$simpath/doc/push.sh    | Push local built Docker containers to the Docker Hub; requires permissions.
$simpath/doc/pull.sh    | Pull Docker containers from the Docker Hub; does *not* require permissions.
$simpath/doc/run.sh     | Run the complete set of Docker containers holding the documentation library
$simpath/doc/stop.sh    | Shutdown the documentation containers.

**PLEASE NOTE** All documentation is also available in PDF format; the files are located in $simpath/doc
