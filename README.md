# sim_release
David J. Sanders, University of Liverpool. As partial fulfillment of the requirements for the degree of Master of Science in Information Technology.

sim_release is the simulation component of David Sanders' MSc in IT Research Project. The hypothesis for the project states that "Context-adaptive privacy in ubiquitous computing can be modelled and simulated in a software environment" and studySim is the simulation component of that project. The simulation framework is a key component.

To run the simulation framework, a number of steps need to be performed:

1. the environment variable $simpath needs to be set:
   export simpath=/path/to/sim_release
   
2. to run the document servers (and test beds): $simpath/doc/run.sh **NOTE** 2GB RAM is required to run the full documentation set. If using Amazon Web Services and a t2.micro instance (which runs the framework really well), only 1GB RAM is available; therefore, the doc servers should be run individually using the -d flag (see *.inc in the $simpath/doc/docsets directory)
   
3. to stop the document servers (and test beds): $simpath/doc/stop.sh **NOTE** As above, if individual sets were run, remember to use the -d flag.

4. to run simulations and reproduce the research output: use $simpath/execute-scenario.sh **NOTE** It can take around an hour per full scenario; use step 5 below to run them individually (perceived as much faster, although the elapsed time is greater).

5. to execute specific scenarios/simulations: look in $simpath/Scenario-Setup and $simpath/Simulation-Sets

Please refer all questions to David Sanders at david.sanders@online.liverpool.ac.uk or dsanderscanada@gmail.com

